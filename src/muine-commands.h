/*
 * Copyright (C) 2004 Mark McLauchlan <filopastry@fastmail.fm>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#include <glib.h>
#include <dbus/dbus-glib.h>

#define MUINE_PLAYING 1
#define MUINE_PREVIOUS 2
#define MUINE_NEXT 3
#define MUINE_VISIBLE 4
#define MUINE_VOLUME 5
#define MUINE_CURRENT_SONG 6
#define MUINE_POSITION 7
#define MUINE_QUEUE 8
#define MUINE_PLAYLIST 9
#define MUINE_PLAY 10
#define MUINE_SONGWINDOW 11
#define MUINE_ALBUMWINDOW 12
#define MUINE_QUIT 13

// events
#define MUINE_SONG_CHANGED 14
#define MUINE_STATE_CHANGED 15

extern DBusGProxy* proxy;

typedef struct Song Song;
struct Song
{
  char *artist;
  char *title;
  char *album;
  int duration;
  int track_number;
};

gboolean connect_to_muine ();

void song_free (Song *s);
Song* song_new ();
Song* get_song_info ();

int muine_info (int cmd);
int muine_command (int cmd);
gboolean play_files (int cmd, char** files, int numfiles);
void muine_set (int cmd, int val);
int muine_cover (char* filename);

void wait_for_signal (int event);
