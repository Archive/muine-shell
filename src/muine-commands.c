/*
 * Copyright (C) 2004 Mark McLauchlan <filopastry@fastmail.fm>
 * Copyright © 2006 Ryan Lortie <desrt@desrt.ca>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#define DBUS_API_SUBJECT_TO_CHANGE

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <glib.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "muine-commands.h"

#define FILENAME_BUFFER_SIZE 512

DBusGProxy* proxy = NULL;
GMainLoop *loop = NULL;

// convert a filename to a full path relative to the cwd
char* get_full_path (char *filename, gboolean mustexist)
{
  struct stat buf; 
  char *cwd, *file = NULL;
  
  if (filename[0] != '/') {
    // no full path given, or else it was just a relative path
    // we need a complete path - try to complete it
    cwd = (char*)g_malloc(FILENAME_BUFFER_SIZE);
    file = (char*)g_malloc(FILENAME_BUFFER_SIZE);
    
    if (getcwd(cwd, FILENAME_BUFFER_SIZE) != NULL) {
      // we got the cwd fine
      if (snprintf(file, FILENAME_BUFFER_SIZE, 
                   "%s/%s", cwd, filename) >= FILENAME_BUFFER_SIZE) {
        // buffer overflow
        g_free(file);
        file = NULL;
      }
      g_free(cwd);
    }
  }
  else
    // no need to do anything
    file = (char*)strdup(filename);

  // check that the file is valid
  if (mustexist && stat(file, &buf) == -1)
    file = NULL;

  return file;
}

void song_free (Song *s)
{
  if (!s) return;

  if (s->artist) g_free(s->artist);
  if (s->title) g_free(s->title);
  if (s->album) g_free(s->album);
  g_free (s);
}

Song* song_new ()
{
  Song *s;

  s = (Song*) g_malloc (sizeof(Song));
  s->artist = NULL; s->title = NULL; s->album = NULL;
  s->duration = 0; s->track_number = 0;

  return s;
}

gboolean connect_to_muine (gboolean init_loop)
{
  DBusGConnection *connection;
  gboolean retval;
  
  if (init_loop)
    g_type_init ();

  connection = dbus_g_bus_get (DBUS_BUS_SESSION, NULL);
  if (connection == NULL) {
    g_printerr("Couldn't connect to DBus - make sure the daemon is running\n");
    proxy = NULL;
    return FALSE;
  }
  else {
    proxy = dbus_g_proxy_new_for_name_owner (connection,
                                             "org.gnome.Muine",
                                             "/org/gnome/Muine/Player",
                                             "org.gnome.Muine.Player",
                                             NULL);
    
    if (proxy == NULL) {
      g_printerr("Muine isn't running\n"); 
      retval = FALSE;
    }
    else
      retval = TRUE;
  }
  
  dbus_g_connection_flush (connection);
  return retval;
}

Song* get_song_info()
{
  char *string, *value;
  char **fields;
  int i;
  Song* song;

  string = (char*) muine_info (MUINE_CURRENT_SONG);
  if (!string || strlen(string) == 0)
    return NULL;

  song = song_new ();
  fields = g_strsplit (string, "\n", 0);

  for(i = 0; fields[i] != NULL; i++) {
    value = strchr(fields[i], ':');
    if (value != NULL && (value + 1) != NULL && (value + 2) != NULL) {
      value += 2;
      if (g_strncasecmp (fields[i], "artist", 6) == 0)
        song->artist = g_strdup (value);
      else if (g_strncasecmp (fields[i], "title", 5) == 0)
        song->title = g_strdup (value);
      else if (g_strncasecmp (fields[i], "album", 5) == 0)
        song->album = g_strdup (value);
      else if (g_strncasecmp (fields[i], "duration", 8) == 0)
        song->duration = atoi (value);
      else if (g_strncasecmp (fields[i], "track_number", 12) == 0)
        song->track_number = atoi (value);
    }
  }
  g_strfreev (fields);
  
  return song;
}

void ensure_connected (void)
{
  if (proxy != NULL)
    return;

  if (!connect_to_muine(TRUE))
    exit (1);
}

int muine_info (int cmd)
{
  DBusGProxyCall *call;
  // TODO: check it's safe to store all return types in an int
  int result;
  int argtype;
  GError *error;

  ensure_connected ();

  // default return (error) values
  switch (cmd) {
  case MUINE_POSITION:
    result = -1;
    break;
  default:
    result = 0;
  }

  switch (cmd) {
  case MUINE_PLAYING:
    call = dbus_g_proxy_begin_call(proxy, "GetPlaying", NULL, NULL, NULL,
                                   G_TYPE_INVALID); 
    argtype = G_TYPE_BOOLEAN;
    break;

  case MUINE_VISIBLE:
    call = dbus_g_proxy_begin_call(proxy, "GetWindowVisible", NULL, NULL, NULL,
                                   G_TYPE_INVALID); 
    argtype = G_TYPE_BOOLEAN;
    break;

  case MUINE_VOLUME:
    call = dbus_g_proxy_begin_call(proxy, "GetVolume", NULL, NULL, NULL,
                                   G_TYPE_INVALID); 
    argtype = G_TYPE_INT;
    break;

  case MUINE_POSITION:
    call = dbus_g_proxy_begin_call(proxy, "GetPosition", NULL, NULL, NULL,
                                   G_TYPE_INVALID); 
    argtype = G_TYPE_INT;
    break;

  case MUINE_CURRENT_SONG:
    call = dbus_g_proxy_begin_call(proxy, "GetCurrentSong", NULL, NULL, NULL,
                                   G_TYPE_INVALID); 
    argtype = G_TYPE_STRING;
    break;
    
  default:
    return result;
  }

  error = NULL;
  dbus_g_proxy_end_call(proxy, call, &error,
                        argtype, &result, G_TYPE_INVALID);
  
  return result;
}

int muine_command(int cmd)
{
  DBusGProxyCall *call;
  gboolean arg;
  GError* error;

  ensure_connected ();

  switch (cmd) {
  case MUINE_NEXT:
    call = dbus_g_proxy_begin_call(proxy, "Next", NULL, NULL, NULL,
                                   G_TYPE_INVALID); 
    break;
  case MUINE_PREVIOUS:
    call = dbus_g_proxy_begin_call(proxy, "Previous", NULL, NULL, NULL,
                                   G_TYPE_INVALID); 
    break;
  case MUINE_SONGWINDOW:
    call = dbus_g_proxy_begin_call(proxy, "PlaySong", NULL, NULL, NULL,
                                   G_TYPE_UINT, 0, G_TYPE_INVALID); 
    break;
  case MUINE_ALBUMWINDOW:
    call = dbus_g_proxy_begin_call(proxy, "PlayAlbum", NULL, NULL, NULL,
                                   G_TYPE_UINT, 0, G_TYPE_INVALID); 
    break;
  case MUINE_QUIT:
    call = dbus_g_proxy_begin_call(proxy, "Quit", NULL, NULL, NULL,
                                   G_TYPE_INVALID); 
    break;
  case MUINE_PLAYING:
    arg = !(gboolean)muine_info(MUINE_PLAYING);
    call = dbus_g_proxy_begin_call(proxy, "SetPlaying", 
                                   NULL, NULL, NULL, G_TYPE_BOOLEAN, arg,
                                   G_TYPE_INVALID); 
    break;
  case MUINE_VISIBLE:
    arg = !(gboolean)muine_info(MUINE_VISIBLE);
    call = dbus_g_proxy_begin_call(proxy, "SetWindowVisible", 
                                   NULL, NULL, NULL, G_TYPE_BOOLEAN, arg,
                                   G_TYPE_UINT, 0, G_TYPE_INVALID); 
    break;
  default:
    return FALSE;
  }
  
  error = NULL;
  return dbus_g_proxy_end_call(proxy, call, &error, G_TYPE_INVALID);
}

gboolean play_files(int cmd, char** files, int numfiles)
{
  int i;
  char* file;
  DBusGProxyCall *call;
  gboolean retval = TRUE;
  
  ensure_connected ();

  for (i = 0; i < numfiles; i++) {
    file = get_full_path(files[i], TRUE);
    if (!file) {
      g_printerr("Failed to open the file %s\n", files[i]);
      retval = FALSE;
    }
    else {
      switch (cmd) {
      case MUINE_PLAYLIST:
        call = dbus_g_proxy_begin_call(proxy, "OpenPlaylist", 
                                       NULL, NULL, NULL, G_TYPE_STRING, file,
                                       G_TYPE_INVALID); 
        break;
        
      case MUINE_QUEUE:
        call = dbus_g_proxy_begin_call(proxy, "QueueFile", 
                                       NULL, NULL, NULL, G_TYPE_STRING, file,
                                       G_TYPE_INVALID); 
        break;
        
      case MUINE_PLAY:
        // play the first song and queue the rest
        if (i == 0)
          call = dbus_g_proxy_begin_call(proxy, "PlayFile", 
                                         NULL, NULL, NULL, G_TYPE_STRING, file,
                                         G_TYPE_INVALID); 
        else
          call = dbus_g_proxy_begin_call(proxy, "QueueFile", 
                                         NULL, NULL, NULL, G_TYPE_STRING, file,
                                         G_TYPE_INVALID); 
        break;
        
      default:
        call = NULL;
        break;
      }

      if (call)
        dbus_g_proxy_end_call(proxy, call, NULL, G_TYPE_INVALID);

      g_free(file);
      retval = TRUE;
    }
  }
  return retval;
}

void muine_set(int cmd, int val)
{
  DBusGProxyCall *call;

  ensure_connected ();
  
  switch (cmd) {
  case MUINE_VOLUME:
    call = dbus_g_proxy_begin_call(proxy, "SetVolume", NULL, NULL, NULL,
                                   G_TYPE_INT, val, DBUS_TYPE_INVALID);
    break;

  case MUINE_POSITION:
    call = dbus_g_proxy_begin_call(proxy, "SetPosition", NULL, NULL, NULL,
                                   G_TYPE_INT, val, DBUS_TYPE_INVALID);
    break;

  default:
    return;
  }
  
  dbus_g_proxy_end_call(proxy, call, NULL, G_TYPE_INVALID);
}

gboolean muine_cover(char* filename)
{
  char *file;
  gboolean retval;
  DBusGProxyCall *call;

  ensure_connected ();

  file = get_full_path(filename, FALSE);
  if (!file) {
    g_printerr("Problem with the filename %s - saving to /tmp/cover.png\n", filename);
    file = strdup("/tmp/cover.png");
  }

  call = dbus_g_proxy_begin_call(proxy, "WriteAlbumCoverToFile", 
                                 NULL, NULL, NULL, G_TYPE_STRING, file, 
                                 G_TYPE_INVALID);
  dbus_g_proxy_end_call(proxy, call, NULL, 
                        G_TYPE_BOOLEAN, &retval, DBUS_TYPE_INVALID);

  g_free(file);
  return retval;
}

static void song_changed_handler (DBusGProxy *proxy,
                                  char *data,
                                  void *user_data)
{
  printf("%s\n", data);
  g_main_loop_quit (loop);
}

static void state_changed_handler (DBusGProxy *proxy,
                                  gboolean data,
                                  void *user_data)
{
  printf("%d\n", data);
  g_main_loop_quit (loop);
}

void wait_for_signal (int event)
{
  ensure_connected ();

  loop = g_main_loop_new (NULL, FALSE);

  switch (event) {
  case MUINE_SONG_CHANGED:
    dbus_g_proxy_add_signal (proxy, "SongChanged", G_TYPE_STRING);
    dbus_g_proxy_connect_signal (proxy, "SongChanged",
                                 G_CALLBACK (song_changed_handler),
                                 NULL, NULL);
    g_main_loop_run (loop);
    break;

  case MUINE_STATE_CHANGED:
    dbus_g_proxy_add_signal (proxy, "StateChanged", G_TYPE_BOOLEAN);
    dbus_g_proxy_connect_signal (proxy, "StateChanged",
                                 G_CALLBACK (state_changed_handler),
                                 NULL, NULL);
    g_main_loop_run (loop);
    break;
  }
}
