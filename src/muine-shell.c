/*
 * Copyright (C) 2004 Mark McLauchlan <filopastry@fastmail.fm>
 * Copyright © 2006 Ryan Lortie <desrt@desrt.ca>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <libgen.h>
#include <stdio.h>
#include <glib.h>
#include <getopt.h>
#include <dbus/dbus-glib.h>

#include "muine-commands.h"

static struct option long_options[] = {
  {"help", 0, 0, 'h'},
  {"version", 0, 0, 'V'},
  {"next", 0, 0, 'n'},
  {"previous", 0, 0, 'p'},
  {"pause", 0, 0, 't'},
  {"visible", 0, 0, 'w'},
  {"queue", 0, 0, 'q'},
  {"play", 0, 0, 'f'},
  {"playlist", 0, 0, 'l'},
  {"song", 0, 0, 's'},
  {"album", 0, 0, 'a'},
  {"volume", 2, 0, 'v'},
  {"position", 2, 0, 'k'},
  {"current-song", 0, 0, 'c'},
  {"cover-image", 0, 0, 'i'},
  {"song-changed", 0, 0, 'o'},
  {"state-changed", 0, 0, 'u'},
  {"quit", 0, 0, 'x'},
  {0, 0, 0, 0}
};

void print_help(char* name)
{
  printf("Usage: %s [options] [file(s)]\n\n", name);
  printf("Options:\n");
  printf("--------\n\n");
  printf(" -h, %-20sDisplay this and exit\n", "--help");
  printf(" -V, %-20sDisplay version and exit\n", "--version");
  printf(" -n, %-20sSkip to next song\n", "--next");
  printf(" -p, %-20sSkip to previous song\n", "--previous");
  printf(" -t, %-20sStart or stop the player\n", "--pause");
  printf(" -w, %-20sShow/hide the playlist window\n", "--visible");
  printf(" -q, %-20sAdd these files to the playlist\n", "--queue");
  printf(" -f, %-20sPlay these files immediately\n", "--play");
  printf(" -l, %-20sOpen this playlist\n", "--playlist");
  printf(" -s, %-20sShow the Song window\n", "--song");
  printf(" -a, %-20sShow the Album window\n", "--album");
  printf(" -v, %-20sSet/show the volume\n", "--volume[=value]");
  printf(" -k, %-20sSet/show position\n", "--position[=value]");
  printf(" -c, %-20sPrint the current song\n", "--current-song");
  printf(" -i, %-20sWrite cover image to a file (default cover.png)\n", "--cover-image");
  printf(" -o, %-20sSleep until the song changes\n", "--song-changed");
  printf(" -u, %-20sSleep until the player is (un)paused\n", "--state-changed");
  printf(" -x, %-20sQuit Muine\n\n", "--quit");
}

int main(int argc, char** argv)
{
  int c;
  int value, offset;
  float delta;
  Song *song;
  char *string;
  int retval;

  retval = 0;

  while (1) {
    c = getopt_long(argc, argv, "hVnptwqflsav::k::cxiou",
                    long_options, NULL);
    if (c == -1)
      break;
    
    switch (c) {      
    case 'n':
      muine_command (MUINE_NEXT);
      break;
      
    case 'p':
      muine_command (MUINE_PREVIOUS);
      break;

    case 't':
      muine_command (MUINE_PLAYING);
      break;

    case 'w':
      muine_command (MUINE_VISIBLE);
      break;

    case 's':
      muine_command (MUINE_SONGWINDOW);
      break;

    case 'a':
      muine_command (MUINE_ALBUMWINDOW);
      break;

    case 'i':
      if (argc == optind) {
        // no argument given - save to the current directory
        if (!muine_cover("cover.png")) {
          printf("Muine couldn't save the image to ./cover.png\n");
          retval = 1;
        }
      }
      else {
        if (!muine_cover(argv[optind])) {
          printf("Muine couldn't save the image to %s\n", argv[optind]);
          retval = 1;
        }
      }
      break;

    case 'v':
      if (optarg) {
        if (optarg[0] == '+' || optarg[0] == '-') {
          value = muine_info (MUINE_VOLUME);
          if (strlen(optarg) > 1) {
            if (optarg[0] == '+')
              value += atoi(&optarg[1]);
            else
              value -= atoi(&optarg[1]);
            if (value > 100)
              value = 100;
            else if (value < 0)
              value = 0;
          }
        }
        else {
          value = atoi(optarg);
        }
        muine_set (MUINE_VOLUME, value);
      }
      else
        printf ("%d\n", muine_info (MUINE_VOLUME));
      break;

    case 'c':
      string = (char*) muine_info (MUINE_CURRENT_SONG);
      if (string && strlen(string) > 0) {
        printf("%s\n", string);
        // string is destroyed by dbus
      }
      break;

    case 'k':
      song = get_song_info ();
      value = muine_info (MUINE_POSITION);
      if (value == -1 || !song)
        exit (1);

      if (optarg) {
        // a single 's' - print the position in seconds
        if (optarg[0] == 's') {
          printf("%d\n", value);
        }
        // a '%' - print a percentage (undocumented feature!)
        else if (optarg[0] == '%') {
          if (song->duration > 0)
            printf("%.2f\n", ((float)value / (float)song->duration) * 100.0);
          else
            printf("0.00\n");
        }
        else {
          offset = (optarg[0] == '+') || (optarg[0] == '-') ? 1 : 0;
          if (optarg[strlen(optarg)-1] == 's')
            delta = atoi(optarg+offset);
          else
            delta = ((float)song->duration * (atof(optarg+offset) / 100.0)) ;

          // relative or absolute position?
          if (optarg[0] == '+')
            muine_set (MUINE_POSITION, value + delta);
          else if (optarg[0] == '-')
            muine_set (MUINE_POSITION, value - delta);
          else
            muine_set (MUINE_POSITION, delta);
        }
      }
      // default - print the percentage
      else {
        if (song->duration > 0)
          printf ("%.2f\n", ((float)value / (float)song->duration) * 100.0);
        else
          printf ("0.00\n");
      }
      song_free (song);
      break;

    case 'x':
      muine_command (MUINE_QUIT);
      break;
      
    case 'q':
      if (argc == optind) {
        printf("** Please list the files to queue\n");
        print_help((char*)basename(argv[0]));
      }
      else
        if (!play_files(MUINE_QUEUE, argv + optind, argc - optind))
          retval = 1;
      break;
      
    case 'f':
      if (argc == optind) {
        printf ("** Please list the files to play\n");
        print_help ((char*)basename(argv[0]));
      }
      else
        if (!play_files(MUINE_PLAY, argv + optind, argc - optind))
          retval = 1;
      break;

    case 'l':
      if (argc == optind) {
        printf ("** Please specify the playlist to load\n");
        print_help ((char*)basename(argv[0]));
      }
      else {
        // there's no point in opening more than one playlist
        if (!play_files(MUINE_PLAYLIST, argv + optind, 1))
          retval = 1;
      }
      break;
      
      // run a command on a signal
    case 'o':
      wait_for_signal (MUINE_SONG_CHANGED);
      break;

    case 'u':
      wait_for_signal (MUINE_STATE_CHANGED);
      break;

    case '?':
    case 'h':
      print_help ((char*)basename(argv[0]));
      break;

    case 'V':
      printf (PACKAGE " " VERSION "\n");
      break;
      
    default:
      printf ("?? Unrecognised option 0%o ??\n", c);
      retval = 1;
    }
  }

  exit(retval);  
}
